from pygost.gost3410 import CURVES
from os import urandom
from os import path
from pygost.gost3410 import prv_unmarshal
from pygost.gost3410 import pub_marshal, pub_unmarshal
from pygost.gost3410 import public_key
from pygost.utils import hexenc, hexdec
from pygost import gost34112012256
from pygost.gost3410 import sign
from pygost.gost3410 import verify
from pygost.gost34112012 import GOST34112012


class Crypto:

    def __init__(self):
        self.curve = CURVES["id-tc26-gost-3410-12-512-paramSetA"]

    def generate_keys(self, ident):
        '''Prv, pub'''
        prv_raw = urandom(32)
        print(prv_raw)
        prv = prv_unmarshal(prv_raw)
        pub = public_key(self.curve, prv)
        pub_file = open(ident + '.pub', 'w')
        prv_file = open(ident + '.prv', 'w')
        pub_file.write(hexenc(pub_marshal(pub, mode=2012)))
        prv_file.write(hexenc(prv_raw))
        pub_file.close()
        prv_file.close()

    def get_pub(self, ident):
        pub_file = open(ident + '.pub', 'r')
        pub = pub_file.read()
        pub_file.close()
        return pub_unmarshal(hexdec(pub), mode=2012)

    def get_prv(self, ident):
        prv_file = open(ident + '.prv', 'r')
        prv = prv_file.read()
        prv_file.close()
        return prv_unmarshal(prv.encode())

    def make_sign(self, prv, message):
        dgst = gost34112012256.new(message).digest() 
        return sign(self.curve, prv, dgst, mode=2012)

    def verify_sign(self, pub, message, signature):
        dgst = gost34112012256.new(message).digest() 
        return verify(self.curve, pub, dgst, signature, mode=2012)

    def encrypt_message(self, text, key):
        alg = GOST3412Kuznechik(key)
        if isinstance(text, str):
            text = text.encode()
        encText = b''
        for i in range(0, len(text), 16):
            st = text[i:i+16]
            if len(st) < 16:
                st = st+b'\x80'*(16-len(st))
            encText += alg.encrypt(st)
        return encText

    def decrypt_massage(self, encText, key):
        alg = GOST3412Kuznechik(key)
        decText = b''
        for i in range(0, len(encText), 16):
            st = encText[i:i+16]
            decText += alg.decrypt(st)
        last = decText[-16:]
        last = last.replace(b'\x80', b'')
        decText = decText[:-16]
        decText += last
        return decText.decode()