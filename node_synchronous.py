import socket
from multiprocessing import Process
from threading import Thread
import json
from os import path
from message_miscs import assemble_message, disassemble_message
from db_miscs import Database
from crypto import Crypto
from pygost.gost3410 import pub_marshal
from pygost.utils import hexenc
from blockchain import Transaction
from blockchain import Blockchain
import random


ports = [6775, 6776, 6777, 6778]


class Network:

    def __init__(self, port, ident):
        self.listening = True
        self.PEERS = {}
        self.id = ident
        self.ip = socket.gethostbyname(socket.gethostname())
        self.port = port
        self.network_config()
        self.BUFFER_SIZE = 2048
        self.keys = {}
        self.db = Database("blockchain_"+self.id+".db")
        self.crypto = Crypto()
        if not path.exists(path.join(self.id + '.pub')):
            self.crypto.generate_keys(self.id)
        self.public_key = self.crypto.get_pub(self.id)
        self.private_key = self.crypto.get_prv(self.id)
        print(self.port)
        print(self.id)
        self.blockchain = Blockchain(self.db)
        self.send_broadcast(assemble_message("hello", self.id, hexenc(pub_marshal(self.public_key, mode=2012)), ""))

    def network_config(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.listenThread = Thread(target=self.listen, name='listenThread')
        self.listenThread.start()

    def listen(self):
        self.BUFFER_SIZE = 2048
        self.sock.bind((self.ip, self.port))
        while self.listening:
            message_raw, addr = self.sock.recvfrom(self.BUFFER_SIZE)
            print("[*] Message has been taken: " + message_raw.decode())
            message = disassemble_message(message_raw)
            if message["flag"] == "hello":
                self.send_message(addr[0], addr[1], assemble_message("responce", self.id, hexenc(pub_marshal(self.public_key)), ''))
                self.db.insert_key(message["node_id"], message["message"])
                self.PEERS[message["node_id"]] = addr[1]
                print('[*] Connected peers: ', list(self.PEERS.items()))
            if message["flag"] == "responce":
                self.db.insert_key(message["node_id"], message["message"])
                self.PEERS[message["node_id"]] = addr[1]
                sync_node = random.choice(list(self.PEERS.items()))
                print('[*] Connected peers: ', list(self.PEERS.items()))
                self.send_message(addr[0], sync_node[1], assemble_message("SYNC_INIT", self.id, '', ''))
            if message["flag"] == "trans":
                print('transaction received')
                self.send_broadcast(assemble_message("AGREE_TRANSACTION", self.id, 'ACCEPTED', ''))
            if message["flag"] == "SYNC_INIT": #Send to user list of block headers
                print('[*] Sync init has been taken, sending blocks...')
                self.send_message(addr[0], addr[1], json.dumps({"flag": "SYNC_REPLY", "node_id": self.id , "message": len(json.dumps({"flag": "SYNC_TRANSFER", "node_id": self.id, "message": json.dumps(self.blockchain.get_blocks_headers())}))}).encode())
            if message["flag"] == "SYNC_REPLY":
                print("in reply")
                self.BUFFER_SIZE = int(message["message"])
                self.send_message(addr[0], addr[1], json.dumps({"flag": "SYNC_READY", "node_id": self.id , "message": ""}).encode())
                print("OK: ", message["message"])
            if message["flag"] == "SYNC_READY":
                self.send_message(addr[0], addr[1], json.dumps({"flag": "SYNC_TRANSFER_HEADERS", "node_id": self.id, "message": self.blockchain.get_blocks_headers()}).encode())
            if message["flag"] == "SYNC_TRANSFER_HEADERS":
                self.BUFFER_SIZE = 2048
                print("Received")
                print(self.BUFFER_SIZE)
                print(message["message"])
                self.send_message(addr[0], addr[1], assemble_message("SYNC_TRANSFERED_HEADERS", self.id, '', '') )
                #write headers to db
            if message["flag"] == "SYNC_TRANSFERED_HEADERS":
                transactions_raw = self.db.get_raw_all_transactions()
                json_obj = assemble_message("SYNC_TRANSFER_TRANSACTION_SIZE", self.id, transactions_raw, '')
                print(json_obj)
                self.send_message(addr[0], addr[1], assemble_message("SYNC_TRANSFER_TRANSACTION_SIZE", self.id, len(json_obj), '') )
            if message["flag"] == "SYNC_TRANSFER_TRANSACTION_SIZE":
                self.BUFFER_SIZE = int(message["message"])
                print(self.BUFFER_SIZE)
                self.send_message(addr[0], addr[1], assemble_message("SYNC_TRANSFER_TRANSACTIONS", self.id, '', ''))
            if message["flag"] == "SYNC_TRANSFER_TRANSACTIONS":
                json_obj = json.dumps({"flag": "SYNC_TRANSFER_TRANSACTIONS_ACCEPT", "node_id": self.id,
                                       "message": self.db.get_raw_all_transactions()})
                self.send_message(addr[0], addr[1], json_obj.encode())

            if message["flag"] == "SYNC_TRANSFER_TRANSACTIONS_ACCEPT":
                pass

    def send_message(self, ip, port, msg):
        self.sock.sendto(msg, (ip, port))

    def get_id(self):
        return self.id

    def send_broadcast(self, msg):
        '''Port broadcast for local tests'''
        for port in ports:
            if port != self.port:
                self.sock.sendto(msg, (self.ip, port))


if __name__ == '__main__':
    #PORT SHIT
    port = input('Type the port of your local node(6775, 6776, 6777, 6778): ')
    ident = input('Type id: ')
    n = Network(int(port), ident)
    command = input('[*] Type command: ')
    while True:
        if command == 'q':
            break
        if command == 'close':
            n.blockchain.close_block()
        if command == 'all':
            print(n.blockchain.get_blockchain())
        command = input('[*] Type command: ')



    #n.send_message('', 6775, assemble_message("trans", n.get_id(), Transaction(0, n.get_id(), 'Vova', 2000).to_json(), ''))
