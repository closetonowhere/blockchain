import sqlite3
import os


class Database:

    def __init__(self, db_name):
        self.db_name = db_name
        self.create_keys_table()

    def create_keys_table(self):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute("select count(name) from sqlite_master where name = 'open_keys' and type = 'table'")
        if db_cursor.fetchone()[0] != 1:
            db_cursor.execute("create table open_keys (name, key)")
            db_connection.commit()
        db_connection.close()

    def create_blocks_table(self):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute("select count(name) from sqlite_master where name = 'blocks' and type = 'table'")
        if db_cursor.fetchone()[0] != 1:
            db_cursor.execute("create table blocks (hash varchar(255), previous_hash varchar(255), merkle_root varchar(255), nonce int, identifier int, time varchar(255))")
            db_cursor.execute("create table if not exists transactions_pool (block_id int, sender varchar(255), receiver varchar(255), amount int)")
            db_cursor.execute("select count(name) from sqlite_master where name = 'transactions' and type = 'table'")
            if db_cursor.fetchone()[0] != 1:
                db_cursor.execute("create table transactions (block_id int, sender varchar(255), receiver varchar(255), amount int)")
            db_connection.commit()
        db_connection.close()

    def update_block(self, field_to_insert, value_to_insert, identifier_to_insert):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute('''update blocks set hash=? where identifier=?''', (value_to_insert, str(identifier_to_insert)))
        db_connection.commit()
        db_connection.close()

    def insert_block(self, block):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute("insert into blocks (hash, previous_hash, merkle_root, nonce, identifier, time) values (?,?,?,?,?,?)", 
                                (block.hash, block.previous_hash, block.merkle_root, block.nonce, block.identifier, block.time))
        db_connection.commit()
        db_connection.close()

    def insert_transaction(self, transaction):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute("insert into transactions (block_id, sender, receiver, amount) values (?,?,?,?)", 
                                (transaction.block_id, transaction.sender, transaction.receiver, transaction.amount))
        db_connection.commit()
        db_connection.close()

    def insert_transaction_into_pool(self, transaction):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute("insert into transactions_pool (block_id, sender, receiver, amount) values (?,?,?,?)", 
                                (transaction.block_id, transaction.sender, transaction.receiver, transaction.amount))
        db_connection.commit()
        db_connection.close()
        
    def get_raw_blocks(self):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute("select * from blocks")
        blocks_raw = db_cursor.fetchall()
        db_connection.close()
        return blocks_raw

    def get_raw_block_transactions(self, block):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute("select * from transactions where block_id=?", (str(block.identifier), ))
        transactions_raw = db_cursor.fetchall()
        db_connection.close()
        return transactions_raw

    def get_raw_all_transactions(self):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute("select * from transactions")
        transactions_raw = db_cursor.fetchall()
        db_connection.close()
        return transactions_raw

    def insert_key(self, name, key):
        db_connection = sqlite3.connect(self.db_name)
        db_cursor = db_connection.cursor()
        db_cursor.execute("insert into open_keys(name, key) values (?, ?)", (name, key))
        db_connection.commit()
        db_connection.close()
