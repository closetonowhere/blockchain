import json
import time
from hashlib import sha256


class Transaction:

    def __init__(self, block_id, sender, receiver, amount):
        self.block_id = block_id
        self.sender = sender
        self.receiver = receiver
        self.amount = amount

    def to_json(self):
        return json.dumps({"block_id": self.block_id, "sender": self.sender, "receiver": self.receiver, "amount": self.amount})

    def to_dict(self, json_object):
        return json.loads(json_object)


class Block:

    def __init__(self, block_hash, previous_hash, merkle_root, nonce, identifier, start_time):
        self.hash = block_hash 
        self.previous_hash = previous_hash
        self.merkle_root = merkle_root
        self.nonce = nonce
        self.identifier = identifier
        self.time = start_time

    def to_json(self):
        return json.dumps({"header": 
                            {
                            "hash": self.hash,
                             "previous_hash": self.previous_hash,
                             "merkle_root": self.merkle_root,
                             "nonce": self.nonce,
                             "identifier": self.identifier,
                             "time": self.time
                             }, 
                           "body": 
                           {
                             "transactions": self.transactions
                           }
                           })

    def to_json_header(self):
        return json.dumps({              
                            "hash": self.hash,
                             "previous_hash": self.previous_hash,
                             "merkle_root": self.merkle_root,
                             "nonce": self.nonce,
                             "identifier": self.identifier,
                             "time": self.time
                             })
      
    def set_hash(self):
        self.hash = sha256(self.to_json_header().encode()).hexdigest()

    def set_transactions(self, transactions):
        self.transactions = transactions

    def get_hash(self):
        return self.hash

    def get_id(self):
        return self.identifier

    def get_previous_hash(self):
        return self.previous_hash


class Blockchain:

    def __init__(self, db):
        "Взять блоки из бд, если нет таблицы, создать ее"
        self.db = db
        self.db.create_blocks_table()
        if len(self.db.get_raw_blocks()) == 0:
            self.db.insert_block(Block("", "", "", 0, 0, 0))  # genesis block, fix hash-function
        self.current_block = self.get_block(-1)
        self.prev_index = self.current_block.get_id()
        if len(self.db.get_raw_blocks()) > 2:
            self.last_hash = self.get_block(-2).get_hash()
        else:
            self.last_hash = ''

    def get_balance(self, identifier):
        transactions_raw = self.db.get_raw_all_transactions()
        transactions = []
        balance = 0
        for transaction_raw in transactions_raw:
            if transaction_raw[1] == identifier:
                balance -= transaction_raw[3]
            if transaction_raw[2] == identifier:
                balance += transaction_raw[3]
        return balance

    def is_transaction_valid(self, transaction):
        if transaction["amount"] > self.get_balance(transaction["sender"]):
            return False
        else:
            return True

    def is_block_valid(self, block):
        previous_hash = self.get_block(block.get_id()-1).get_hash()
        if block.get_previous_hash != previous_hash:
            return False
        if block.get_hash() != sha256(block.to_json_header().encode()).hexdigest():
            return False
        #Merkle root check
        #if block.get_merkle_root() != self.count_merkle_root(block):
            #return False
        return True

    def is_blockchain_valid(self):
        blockchain = self.get_blockchain()
        for block in blockchain:
            if not self.is_block_valid(block):
                return False
        return True


    def get_block(self, number):
        last_block_raw = self.db.get_raw_blocks()[number]
        block = Block(last_block_raw[0], last_block_raw[1], last_block_raw[2], last_block_raw[3], last_block_raw[4], last_block_raw[5])
        block.set_transactions(self.get_block_transactions(block))
        return block

    def get_blocks_headers(self):
        '''Only Block headers'''
        raw_blocks = self.db.get_raw_blocks()
        blocks = []
        size = 0
        for raw_block in raw_blocks:
            block = {
                            "hash": raw_block[0],
                             "previous_hash": raw_block[1],
                             "merkle_root": raw_block[2],
                             "nonce": raw_block[3],
                             "identifier": raw_block[4],
                             "time": raw_block[5]
                             }
            size += len(json.dumps(block))
            blocks.append(block)
        return blocks

    def get_blockchain(self):
        '''Blocks with tranactions'''
        blockchain = []
        raw_blocks = self.db.get_raw_blocks()
        for raw_block in raw_blocks:
            block = Block(raw_block[0], raw_block[1], raw_block[2], raw_block[3], raw_block[4], raw_block[5])
            block.set_transactions(self.get_block_transactions(block))
            blockchain.append(block)
        return blockchain

    def get_block_transactions(self, block):
        transactions_raw = self.db.get_raw_block_transactions(block)
        transactions = []
        for transaction_raw in transactions_raw:
            transactions.append(Transaction(transaction_raw[0], transaction_raw[1], transaction_raw[2], transaction_raw[3]))
        return transactions

    def count_merkle_root(self, block):
        pass
        
    def close_block(self):
        self.current_block.set_hash()
        self.db.update_block('hash', self.current_block.get_hash(), self.current_block.get_id())
        self.last_hash = self.current_block.get_hash()
        self.db.insert_block(Block('', self.last_hash, '', 0, int(self.prev_index) + 1, 0))
        self.current_block = self.get_block(-1)
        self.prev_index = self.prev_index + 1
        
    def make_transaction_into_pool(self, sender, receiver, amount):
        self.db.insert_transaction_into_pool(Transaction(0, sender, receiver, amount))

