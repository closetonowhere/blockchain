import json


def assemble_message(flag, id, message, signature):
    return json.dumps({"flag": flag, "node_id": id, "message": message, "signature": signature}).encode()
            

def disassemble_message(raw):
    return json.loads(raw.decode())
